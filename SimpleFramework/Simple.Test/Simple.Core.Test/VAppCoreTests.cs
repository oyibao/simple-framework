using NUnit.Framework;
using Simple.Utils.Helper;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Simple.Utils.Test
{
    public class SimpleCoreTests
    {
        [SetUp]
        public void Setup()
        {
            ConfigHelper.Init(new string[] { "appsettings.json" }, false, true);
        }

        [Test]
        public void TcpLogTest()
        {
            for (int i = 0; i < 20; i++)
            {
                ConsoleHelper.Debug($"[TcpLogTest]，发送日志【{i}】当前线程 {Thread.CurrentThread.ManagedThreadId}");
            }

            Thread.Sleep(20 * 1000);
        }

        [Test]
        public void NoLockTest()
        {
            var tasks = new List<Task>();
            for (int i = 0; i < 10; i++)
            {
                tasks.Add(Task.Run(() =>
                {
                    Thread.Sleep(TimeSpan.FromSeconds(1));
                    ConsoleHelper.Debug($"[Nolock]当前线程 {Thread.CurrentThread.ManagedThreadId}");
                }));
            }
            Task.WaitAll(tasks.ToArray());
        }

        [Test]
        public void LockHelperTest()
        {
            var keyDic = new string[] { "a", "b" };
            var rand = new Random();
            var tasks = new List<Task>();

            LockHelper.MessageEv += msg =>
            {
                ConsoleHelper.Debug(msg);
            };

            var key = $"ssss_{rand.Next(0, 5)}";
            for (int i = 0; i < 5; i++)
            {
                new Thread(() =>
                {
                    while (true)
                    {
                        Thread.Sleep(1000);
                        try
                        {
                            LockHelper.LockRun(key, act: () =>
                            {
                                Thread.Sleep(TimeSpan.FromSeconds(10));
                                ConsoleHelper.Debug($"[{key}]执行了一次函数，当前执行线程 {Thread.CurrentThread.ManagedThreadId}");
                            }, 5);
                        }
                        catch (Exception ex)
                        {
                            ConsoleHelper.Debug($"线程 {Thread.CurrentThread.ManagedThreadId}，[lock]异常 {ex.Message}");
                        }
                    }
                }).Start();
            }
            Console.ReadLine();
        }

        [Test]
        public void ObjectTest()
        {
            var temp = new Temp { x = 1, y = "2", z = "哈哈", tim = DateTime.Now };
            ConsoleHelper.Debug("1: " + JsonHelper.ToJson(temp));
            temp.Next(s => s.x = 2);
            ConsoleHelper.Debug("2: " + JsonHelper.ToJson(temp));
            temp.Next(x => x.tim = DateTime.Now);
            ConsoleHelper.Debug("3: " + JsonHelper.ToJson(temp));

            Console.ReadLine();
        }

        [Test]
        public void SnowFlakeHelperTest()
        {
            var maxCount = 100000;
            long preId = 0;

            for (int i = 0; i < maxCount; i++)
            {
                var id = IdHelper.GetId();
                Assert.True(id > preId);
                preId = id;
                //LogHelper.Debug("Id记录:" + id.ToString());
            }

            var ids = new List<long>();
            Parallel.For(0, 10, i =>
            {
                for (var j = 0; j < maxCount; j++)
                {
                    var id = IdHelper.GetId();
                    Assert.AreEqual(id > 0, true);
                    lock (ids)
                    {
                        ids.Add(id);
                    }
                }
            });
            Assert.AreEqual(ids.Count, 10 * maxCount);
        }

        [Test]
        public void LogTest()
        {
            var ex = new Exception("测试的异常");
            LogHelper.Info("info", ex);
            LogHelper.Warn("Warn", ex);
            LogHelper.Debug("Debug", ex);
            LogHelper.Error("Error", ex);
            LogHelper.Fatal("Fatal", ex);
        }

        public class Temp
        {
            public int x { get; set; } = 1;
            public string y { get; set; } = "2";
            public string z { get; set; } = "哈哈";
            public DateTime tim { get; set; } = DateTime.Now;
        }
    }
}
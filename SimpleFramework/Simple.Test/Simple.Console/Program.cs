﻿// See https://aka.ms/new-console-template for more information
using System.Linq.Expressions;
using Simple.Console;
using Simple.Console.Test;
using Simple.Utils;
using Simple.Utils.Helper;

ConfigHelper.Init(new string[] { "appsettings.json" }, false, true);

TestHtmlPdf.TestAsync();